package com.mxc.split.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

@TableName("t_product")
@Data
@EqualsAndHashCode(callSuper = false)
public class Product extends Model<Product> {
	private static final long serialVersionUID = -3771227875479811383L;
	private long id;
	private String name;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
}
