package com.mxc.split.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mxc.split.entity.Product;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductMapper extends BaseMapper<Product> {

    Product queryById(@Param("id") int id);


    List<Product> list(@Param("name") String name);
}
