package com.mxc.split.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MyUtil {
    public static String currentDateTime(){
        LocalDateTime localDateTime=LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String now = dateTimeFormatter.format(localDateTime);
        return now;
    }

    /**
     * 根据自增id生成唯一订单号
     * @param incrKey 自增id
     * @return
     */
    public static String renderOrderId(Long incrKey){
        /**
         * 日期+00001 后边是5位数 补零
         */
        String orderId = currentDateTime() + String.format("%1$05d", incrKey);
        return orderId;
    }

    public static void main(String[] args) {
        String result = renderOrderId(2L);
        System.out.println(result);
    }

}
