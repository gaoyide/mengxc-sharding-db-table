package com.mxc.split.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mxc.split.dao.ProductMapper;
import com.mxc.split.entity.Product;
import com.mxc.split.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

	@Autowired
	private ProductMapper productMapper;

	@Override
	public int saveProduct(Product product) {
		System.out.println(product.toString());
		return baseMapper.insert(product);
	}

	@Override
	public List<Product> getAllProducts() {
		QueryWrapper<Product> qw = new QueryWrapper<Product>();
		qw.between("create_time", "2020-05-12 00:00:00", "2020-10-01 00:00:00");
		List<Product> list = baseMapper.selectList(qw);
		return list;
	}

	@Override
	public List<Product> list(String name) {
		QueryWrapper<Product> qw = new QueryWrapper<Product>();
		qw.between("create_time", "2020-05-12 00:00:00", "2020-10-01 00:00:00");
		qw.eq("name", name);
		return baseMapper.selectList(qw);
	}

	@Override
	public Product selectById(Long id) {
		Product s = productMapper.selectById(id);
		return s;
	}
}
