package com.mxc.split.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mxc.split.entity.Product;

import java.util.List;

public interface ProductService extends IService<Product> {

    public int saveProduct(Product product);
    public List<Product> getAllProducts();
    public List<Product> list(String name);

    public Product selectById(Long id);



}
