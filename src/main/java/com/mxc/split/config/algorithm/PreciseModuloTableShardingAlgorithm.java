package com.mxc.split.config.algorithm;

import org.apache.commons.lang3.StringUtils;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * @ClassName: PreciseModuloTableShardingAlgorithm
 * @Description: 分表的自定义规则类(精确)
 * @author mengxc
 * @date 2020年9月29日 下午4:00:01
 */
public class PreciseModuloTableShardingAlgorithm implements PreciseShardingAlgorithm<Date> {
	@Override
	public String doSharding(Collection<String> collection, PreciseShardingValue<Date> preciseShardingValue) {
		// 对于库的分片collection存放的是所有的库的列表，这里代表t_product_01~t_product_12
		// 配置的分片的sharding-column对应的值
		Date createTime = preciseShardingValue.getValue();
		String timeValue = new SimpleDateFormat("yyyyMMddHHmmss").format(createTime);
		// 分库时配置的sharding-column
//		String time = preciseShardingValue.getColumnName();
		// 需要分库的逻辑表
//		String table = preciseShardingValue.getLogicTableName();
		if (StringUtils.isBlank(timeValue)) {
			throw new UnsupportedOperationException("精确分片值为NULL;");
		}
		// 按月路由
		for (String each : collection) {
			String value = StringUtils.substring(timeValue, 4, 6); // 获取到月份
			System.out.println("each==" + each);
			if (each.endsWith(value)) {
				// 这里返回回去的就是最终需要查询的表名
				return each;
			}
		}
		return null;
	}
}