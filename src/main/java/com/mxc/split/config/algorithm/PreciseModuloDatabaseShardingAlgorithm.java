package com.mxc.split.config.algorithm;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

/**
 * @ClassName: PreciseModuloDatabaseShardingAlgorithm
 * @Description: 分库的自定义类(精确)
 * @author mengxc
 * @date 2020年9月29日 下午3:58:46
 */
public class PreciseModuloDatabaseShardingAlgorithm implements PreciseShardingAlgorithm<Date> {
	/**
	 * @param collection 存放的是所有的库的列表
	 * @return 将数据写入的哪个库
	 */
	@Override
	public String doSharding(Collection<String> collection, PreciseShardingValue<Date> preciseShardingValue) {
		// 对于库的分片collection存放的是所有的库的列表，这里代表db-test-2019~db-test-2023
		// 配置的分片的sharding-column对应的值
		Date createTime = preciseShardingValue.getValue();
		String timeValue = new SimpleDateFormat("yyyyMMddHHmmss").format(createTime);
		// 分库时配置的sharding-column
//		String time = preciseShardingValue.getColumnName();
		// 需要分库的逻辑表
//		String table = preciseShardingValue.getLogicTableName();
		if (StringUtils.isBlank(timeValue)) {
			throw new UnsupportedOperationException("精确分片值为NULL;");
		}
		// 按年路由
		for (String each : collection) {
			String value = StringUtils.substring(timeValue, 0, 4); // 获取到年份
			if (each.endsWith(value)) {
				// 这里返回回去的就是最终需要查询的库名
				return each;
			}
		}
		throw new UnsupportedOperationException();
	}
}